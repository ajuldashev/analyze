package com.mycompany.analyzed;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Analyzer 
{
    // Время начала интервала
    private String begin_time;
    // Время окончания интервала
    private String end_time;
    // Текущее время обработки, нужно для подсчета элементов в нутри одной секунды
    private String current_time;

    // общее кол-во запросов
    private int qty_N;
    // Удачное кол-во запросов
    private int qty_n;

    // Общее кол-во запросов за последнею секунду 
    private int last_N;
    // Удачное кол-во запросов за последнею секунду 
    private int last_n;

    // Максимальное допустимое время ответа
    private float time;
    // Минимально уровень доступности
    private float availability_level;
    // включен ли второй алгоритм
    private boolean algorithm_v2;
    
    // Внутренней класс нужен для хранения интервалов, чтобы их можно было потом отсортировать
    private class Interval 
    {
        public String begin_time;
        public String end_time;
        public float availability_level;
        Interval(String begin_time_t, String end_time_t, float availability_level_t) {
            begin_time = begin_time_t;
            end_time = end_time_t;
            availability_level = availability_level_t;
        }
    }

    // Место хранения интервалов
    private ArrayList<Interval> list;

    Analyzer(float time_t, float availability_level_t, boolean algorithm_v2_t){
        begin_time = null;
        end_time = null;
        current_time = null;
        qty_N = 0;
        qty_n = 0;
        last_N = 0;
        last_n = 0;
        time = time_t;
        availability_level = availability_level_t;
        algorithm_v2 = algorithm_v2_t;
        list = new ArrayList<Interval>();
    }

    // Функция для обработки запросов из потока
    public void start(BufferedReader reader) {
        String line = null;
        try {
            // читаем и обрабатываем каждую строку
            line = reader.readLine();
            while (line != null) {
                pars_line(line);
                line = reader.readLine();
            }
            // Если остался не обработанный интервал
            end();
            // сортируем и выводим
            sort();
            print();
        } catch (java.io.IOException e) {
            System.err.println("Error read line");
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            System.err.println("Error pars line");
        }
    }

    private void pars_line(String line) {
        // получаем значение текущего времени, нужно для подсчетов запросов за одну секунду
        String temp_time = get_field(line, 3);
        // Проверяем сменилась ли время
        if (!temp_time.equals(current_time)) {
            current_time = temp_time;
            if (end_time != null) {
                // Продолжаем считать старый интервал
                // вычисляем сколько всего было запросов за текущий интервал и сколько было из них удачным
                int new_N = qty_N + last_N;
                int new_n = qty_n + last_n;
                // Получаем уровень доступности за последнею секунду 
                float last_u = get_level(last_n, last_N);
                // Получаем уровень доступности за весь последний интервал
                float new_u = get_level(new_n, new_N);
                // Проверяем в зависимости от типа алгоритма на выход из интервала
                boolean f = (!algorithm_v2)&&(new_u >= availability_level) || (algorithm_v2)&&(last_u >= availability_level);
                if (f) {
                    // завершаем интервал 
                    // вычисляем уровень доступности и добавляем интервал в список
                    float u = get_level(qty_n, qty_N);
                    add_interval(begin_time, end_time, u);
                    begin_time = null;
                    end_time = null;
                    // сбрасываем счетчики 
                    qty_N = 0;
                    qty_n = 0;
                } else {
                    // продолжаем подсчет
                    qty_N = new_N;
                    qty_n = new_n;
                    end_time = current_time;
                }
            } else {
                // начинаем считать новый интервал 
                float last_u = get_level(last_n, last_N);
                if (last_u < availability_level) {
                    if (begin_time == null) {
                        begin_time = temp_time;
                    }
                    end_time = temp_time;
                    qty_N = last_N;
                    qty_n = last_n;

                }
            }
            // сбрасываем счетчики за последнею секунду 
            last_N = 0;
            last_n = 0;
        }
        // подсчитываем кол-во всего запросов и сколько из них удачных
        last_N += 1;
        if (test_line(line, time)) {
           last_n += 1;
        }
    }
    // Проверка и добавление последнего интервала
    private void end(){
        if (end_time != null) {
            int new_N = qty_N + last_N;
            int new_n = qty_n + last_n;
            float new_u = get_level(new_n, new_N);
            if (new_u < availability_level) {
                float u = get_level(qty_n, qty_N);
                add_interval(begin_time, end_time, u);
            }
        }
    }
    // Вычисляем уровень доступности по данным за интервал, N - общее кол-во запросов, n - удачное кол-во запросов
    public float get_level(float n, float N) {
        return (n/N)*100;
    }
    // Добавляем интервал в список
    public void add_interval(String beg_time, String end_time, float u) {
        list.add(new Interval(to_time(beg_time), to_time(end_time), u));
    }
    // Отбрасываем дату у времени
    public String to_time(String t) {
        String [] strs = t.split(":");
        return strs[1] + ":" + strs[2] + ":" + strs[3];
    }
    //Получаем поле из запроса
    public String get_field(String str, int pos) {
        return str.split(" ")[pos];
    }
    // Проверяем удачный ли запрос
    public boolean test_line(String str, float t) {
        String code = get_field(str, 8);
        float time = Float.valueOf(get_field(str, 10));
        return (code.charAt(0) != '5' && code.length() == 3) && (time < t);
    }
    // сортировка интервалов
    public void sort() {
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object arg0, Object arg1) {
                Interval i1 = (Interval)(arg0);
                Interval i2 = (Interval)(arg1);
                return i1.begin_time.compareTo(i2.begin_time);
            }
        });
    }
    // печать из списка интервалов
    public void print() {
        for (int i = 0; i < list.size(); ++i) {
            Interval interval = (Interval)list.get(i);
            System.out.println(interval.begin_time + " " + interval.end_time + " " + interval.availability_level);
        }
    }
}

