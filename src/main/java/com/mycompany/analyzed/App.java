package com.mycompany.analyzed;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class App 
{
    public static void main( String[] args )
    {
        // Максимальное время ответа 
        float time = 0;
        // Минимальный уровень доступности 
        float availability_level = 0;
        // 2-й варисант работы алгоритма расчета интервалов, 
        // интервал считается завершенным если встречается секунда 
        // в течение которой уровень доступности превышает минимально допустимый 
        boolean algorithm_v2 = false;
        // обработка флагов
        for (int i = 0; i < args.length; ++i) {
            switch(args[i]) {
            // флаг задающий максимальное время ответа 
            case "-t":
                time = Float.valueOf(args[i + 1]);
                i += 1;
                break;
            // флаг задающий минимальный уровень доступности
            case "-u":
                availability_level = Float.valueOf(args[i + 1]);
                i += 1;
                break;
            // флаг включающий второй алгоритм
            case "-v2":
                algorithm_v2 = true;
                break;
            }
        }
        // Запуск анализа входящего потока данных
        Analyzer a = new Analyzer(time, availability_level, algorithm_v2);
        a.start(new BufferedReader(new InputStreamReader(System.in)));
    }
}

