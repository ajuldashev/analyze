package com.mycompany.analyzed;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
        Analyzer a = new Analyzer(45, 99, false);
        assertTrue(a.test_line("192.168.32.181 - - [14/06/2017:16:47:02 +1000] \"PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1\" 200 2 44.510983 \"-\" \"@list-item-updater\" prio:0", 45));
        assertTrue(!(a.test_line("192.168.32.181 - - [14/06/2017:16:47:02 +1000] \"PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1\" 200 2 54.510983 \"-\" \"@list-item-updater\" prio:0", 45)));
        assertTrue(!(a.test_line("192.168.32.181 - - [14/06/2017:16:47:02 +1000] \"PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1\" 500 2 44.510983 \"-\" \"@list-item-updater\" prio:0", 45)));
        assertTrue(a.test_line("192.168.32.181 - - [14/06/2017:16:47:02 +1000] \"PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1\" 600 2 44.510983 \"-\" \"@list-item-updater\" prio:0", 45));

        String str = "192.168.32.181 - - [14/06/2017:16:47:02 +1000] \"PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1\" 200 2 44.510983 \"-\" \"@list-item-updater\" prio:0";
        String str2 = "192.168.32.181";
        assertTrue(str2.equals(a.get_field(str, 0)));
        str2 = "[14/06/2017:16:47:02";
        assertTrue(str2.equals(a.get_field(str, 3)));
        str2 = "200";
        assertTrue(str2.equals(a.get_field(str, 8)));
        str2 = "44.510983";
        assertTrue(str2.equals(a.get_field(str, 10)));
        
        str2 = "16:47:02";
        assertTrue(str2.equals(a.to_time(a.get_field(str, 3))));

    }
}
